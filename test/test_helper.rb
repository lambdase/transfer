ENV["RAILS_ENV"] ||= "test"
require_relative "../config/environment"
require "rails/test_help"
require "database_cleaner"
require "minitest/autorun"
require "ostruct"

DatabaseCleaner.strategy = :transaction

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...

  def with_manager(response)
    TransactionManager.stub :new, MockTransactionManager.new(response) do
      yield
    end
  end
end

class ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
end

module DatabaseCleaner
  def before_setup
    super
    DatabaseCleaner.start
  end

  def after_teardown
    super
    DatabaseCleaner.clean
  end
end

class MockTransactionManager
  def initialize(response)
    @response = response
  end

  def transfer_to(amount, other_account_number)
    @response
  end

  def available
    0
  end

  def account_number
    "a number"
  end

  def transactions
    [OpenStruct.new(id: 1, transaction_type: :deposit, amount: 100)]
  end
end
