require "test_helper"

class TransactionManagerTest < ActiveSupport::TestCase
  include DatabaseCleaner

  test "#available reports funds available" do
    user = User.create(email: "foo@bar.com", password: "123456")

    transaction_manager = TransactionManager.new(user)

    transaction_manager.deposit(100)
    transaction_manager.deposit(200)
    transaction_manager.deposit(300)
    transaction_manager.withdraw(15)

    assert_equal 585, transaction_manager.available
  end

  test "#transactions reports most recent transactions last" do
    user = User.create(email: "foo@bar.com", password: "123456")
    transaction_manager = TransactionManager.new(user)

    trans_1 = transaction_manager.deposit(100)
    trans_2 = transaction_manager.deposit(300)
    trans_3 = transaction_manager.withdraw(15)
    trans_4 = transaction_manager.withdraw(30)

    assert_equal [trans_1, trans_2, trans_3, trans_4], transaction_manager.transactions
  end

  test "#transfer_to transfers between account" do
    user_1 = User.create(email: "foo@bar.com", password: "123456")
    user_2 = User.create(email: "qux@corge.com", password: "123456")

    transaction_manager_1 = TransactionManager.new(user_1)
    transaction_manager_2 = TransactionManager.new(user_2)

    transaction_manager_1.deposit(100)
    transaction_manager_1.deposit(150)

    assert_equal 250, transaction_manager_1.available

    res = transaction_manager_1.transfer_to(100, transaction_manager_2.account_number)

    assert_equal 150, transaction_manager_1.available

    assert_equal :ok, res

    assert_equal 100, transaction_manager_2.available
  end

  test "#transfer_to verifies funds are sufficient" do
    user_1 = User.create(email: "foo@bar.com", password: "123456")
    user_2 = User.create(email: "qux@corge.com", password: "123456")

    transaction_manager_1 = TransactionManager.new(user_1)
    transaction_manager_1.deposit(100)

    transaction_manager_2 = TransactionManager.new(user_2)

    assert_equal :insufficient_funds, transaction_manager_1.transfer_to(200, transaction_manager_2.account_number)
    assert_equal 0, transaction_manager_2.available
  end

  test "#transfer_to prevents same account transfers" do
    user_1 = User.create(email: "foo@bar.com", password: "123456")

    transaction_manager_1 = TransactionManager.new(user_1)
    transaction_manager_1.deposit(300)

    assert_equal :same_account, transaction_manager_1.transfer_to(200, transaction_manager_1.account_number)
  end

  test "#transfer_to verifies other account existence" do
    user_1 = User.create(email: "foo@bar.com", password: "123456")

    transaction_manager_1 = TransactionManager.new(user_1)
    transaction_manager_1.deposit(300)

    assert_equal :account_not_found, transaction_manager_1.transfer_to(200, "does not exist")
  end

  test "#transfer_to rejects negative transfers" do
    user_1 = User.create(email: "foo@bar.com", password: "123456")
    user_2 = User.create(email: "qux@corge.com", password: "123456")

    transaction_manager_1 = TransactionManager.new(user_1)
    transaction_manager_2 = TransactionManager.new(user_2)
    transaction_manager_1.deposit(300)

    assert_equal :invalid_amount, transaction_manager_1.transfer_to(-1, transaction_manager_2.account_number)
    assert_equal 300, transaction_manager_1.available
  end
end
