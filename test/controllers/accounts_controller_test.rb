require "test_helper"

class AccountsControllerTest < ActionDispatch::IntegrationTest
  include DatabaseCleaner

  def setup
    @user = User.create(email: "foo@bar.com", password: "123456")

    sign_in(@user)
  end

  test "render account information" do
    TransactionManager.stub :new, MockTransactionManager.new(:ok) do
      get account_path
      assert_response :success
    end
  end
end
