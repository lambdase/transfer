require "test_helper"

class TransfersControllerTest < ActionDispatch::IntegrationTest
  include DatabaseCleaner

  def setup
    @user = User.create(email: "foo@bar.com", password: "123456")

    sign_in(@user)
  end

  test "validates amount is positive" do
    with_manager(:ok) do
      post transfers_path, params: { amount: -1, destination_account: "foobar"  }
      assert_response :bad_request
    end
  end

  test "validates amount is an integer" do
    with_manager(:ok) do
      post transfers_path, params: { amount: 1.33, destination_account: "foobar"  }
      assert_response :bad_request
    end
  end

  test "validates destination account is present" do
    with_manager(:ok) do
      post transfers_path, params: { amount: 10 }
      assert_response :bad_request
    end
  end

  test "redirects to home on success" do
    with_manager(:ok) do
      post transfers_path, params: { amount: 10, destination_account: "foobar" }
      assert_redirected_to root_path
    end
  end

  test "reports failure on insufficient funds" do
    with_manager(:insufficient_funds) do
      post transfers_path, params: { amount: 10, destination_account: "foobar" }
      assert_response :bad_request

      assert_equal flash[:alert][:amount], "Not enough funds for transfer"
    end
  end

  test "reports failure when transferring to own account" do
    with_manager(:same_account) do
      post transfers_path, params: { amount: 10, destination_account: "foobar" }
      assert_response :bad_request

      assert_equal flash[:alert][:amount], "Must transfer to a different account"
    end
  end

  test "reports failure on missing account" do
    with_manager(:account_not_found) do
      post transfers_path, params: { amount: 10, destination_account: "foobar" }
      assert_response :bad_request

      assert_equal flash[:alert][:destination_account], "No such account"
    end
  end

  test "reports failure on invalid transfer amount" do
    with_manager(:invalid_amount) do
      post transfers_path, params: { amount: 10, destination_account: "foobar" }
      assert_response :bad_request

      assert_equal flash[:alert][:destination_account], "Invalid transfer amount"
    end
  end
end
