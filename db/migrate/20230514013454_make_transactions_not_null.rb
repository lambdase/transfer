class MakeTransactionsNotNull < ActiveRecord::Migration[7.0]
  def change
    change_column_null :transactions, :account_id, false
    change_column_null :transactions, :transaction_type, false
    change_column_null :transactions, :amount, false
  end
end
