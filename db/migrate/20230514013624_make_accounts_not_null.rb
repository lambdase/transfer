class MakeAccountsNotNull < ActiveRecord::Migration[7.0]
  def change
    change_column_null :accounts, :account_number, false
    change_column_null :accounts, :user_id, false
  end
end
