class CreateTransactions < ActiveRecord::Migration[7.0]
  def change
    create_table :transactions do |t|
      t.references :account, index: true, foreign_key: true
      t.string :transaction_type
      t.bigint :amount

      t.timestamps
    end
  end
end
