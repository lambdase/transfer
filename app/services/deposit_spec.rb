class DepositSpec
  include ActiveModel::Validations

  def initialize(params)
    @amount = params[:amount]
  end

  attr_accessor :amount

  validates :amount, presence: true, numericality: { only_integer: true, greater_than: 0 }
end
