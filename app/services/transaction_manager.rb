class TransactionManager
  def initialize(user)
    @user_account = Account.find_or_create_by(user: user) do |account|
      account.account_number = SecureRandom.hex
    end
  end

  def deposit(amount)
    Transaction.create!(account: @user_account, transaction_type: "deposit", amount: amount)
  end

  def withdraw(amount)
    Transaction.create!(account: @user_account, transaction_type: "withdrawal", amount: amount)
  end

  def available
    funds_available = Transaction.where(account_id: @user_account.id).group(:transaction_type).sum(:amount)

    funds_available.fetch("deposit", 0) - funds_available.fetch("withdrawal", 0)
  end

  def account_number
    @user_account.account_number
  end

  def transactions
    @user_account.transactions.order(created_at: :asc)
  end

  def transfer_to(amount, other_account_number)
    return :invalid_amount unless amount.to_i > 0
    return :same_account if @user_account.account_number == other_account_number

    ActiveRecord::Base.transaction do
      if amount.to_i < available
        other_account = Account.find_sole_by(account_number: other_account_number)

        Transaction.create!(account: @user_account, transaction_type: "withdrawal", amount: amount)
        Transaction.create!(account: other_account, transaction_type: "deposit", amount: amount)

        :ok
      else
        :insufficient_funds
      end
    end
  rescue ActiveRecord::RecordNotFound
    :account_not_found
  end
end
