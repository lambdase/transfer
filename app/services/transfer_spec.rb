class TransferSpec
  include ActiveModel::Validations

  def initialize(params)
    @amount = params[:amount]
    @destination_account = params[:destination_account]
  end

  attr_accessor :amount, :destination_account

  validates :amount, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :destination_account, presence: true
end
