class AccountsController < ApplicationController
  before_action :authenticate_user!

  def show
    transaction_manager = TransactionManager.new(current_user)

    @available = transaction_manager.available
    @transactions = transaction_manager.transactions
    @account_number = transaction_manager.account_number
  end
end
