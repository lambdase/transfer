class DepositsController < ApplicationController
  before_action :authenticate_user!

  def create
    deposit_spec = DepositSpec.new(deposit_params)

    if deposit_spec.invalid?
      flash[:alert] = deposit_spec.errors.messages

      redirect_to new_deposit_path and return
    end

    transaction_manager = TransactionManager.new(current_user)

    transaction_manager.deposit(params[:amount])

    redirect_to root_path
  end

  private

  def deposit_params
    params.permit(:amount)
  end
end
