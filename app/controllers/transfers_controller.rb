class TransfersController < ApplicationController
  before_action :authenticate_user!

  def create
    transfer_spec = TransferSpec.new(transfer_params)

    if transfer_spec.invalid?
      flash[:alert] = transfer_spec.errors.messages

      render :new, status: :bad_request and return
    end

    transaction_manager = TransactionManager.new(current_user)

    out = transaction_manager.transfer_to(transfer_spec.amount, transfer_spec.destination_account)

    if out == :ok
      flash[:notice] = "Transfer succeeded"
      redirect_to root_path

    elsif out == :insufficient_funds
      flash[:alert] = { amount:  "Not enough funds for transfer" }
      render :new, status: :bad_request and return

    elsif out == :same_account
      flash[:alert] = { amount:  "Must transfer to a different account" }
      render :new, status: :bad_request and return

    elsif out == :account_not_found
      flash[:alert] = { destination_account:  "No such account" }
      render :new, status: :bad_request and return

    elsif out == :invalid_amount
      flash[:alert] = { destination_account:  "Invalid transfer amount" }
      render :new, status: :bad_request and return
    end

  end

  private

  def transfer_params
    params.permit(:amount, :destination_account)
  end
end
