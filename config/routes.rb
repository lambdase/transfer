Rails.application.routes.draw do
  root "accounts#show"

  resource :account, only: [:show]
  resources :deposits, only: [:new, :create]
  resources :transfers, only: [:new, :create]

  devise_for :users
end
