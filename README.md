# README

## How to run this

This is a normal Rails application with ERB-based templates. The app is
configured to use SQLite as the database. Assuming a recent version of Ruby, the
following steps should start the application.

1. Install dependencies with bundler

```
bundle install
```

2. Prepare the database

```
bundle exec rails db:prepare
```


4. Run the server in development mode

```
bundle exec rails s
```

## Running the tests

1. Prepare the test database

```
bundle exec rails db:test:prepare
```

2. Run the test suite

```
bundle exec rails test
```

## Usage notes

1. To use the application, you must create at least two accounts. Each user has
a single account that is created idempotently when loading the homepage. The
account number (note this is distinct from the account ID) is generated
automatically and can be used for transfers between users.

## Implementation notes

1. This is implemented using a ledger. For performance, real financial
applications often consolidate the current account balance on a separate column
(for instance in the `accounts` table). After a certain number of transactions,
an scheduled task consolidates all transactions into a single balance to prevent
the transactions tables from growing without bounds. For simplicity sake’s, this
hasn’t been implemented here. If most users have a small amount of transactions
*each*, the indices on account-related columns in `transactions` should keep the
app performant.

2. SQLite was used to make it easy to run and test the app. For a real
application, a multiuser database like PostgreSQL should be used. The app
does not depend on SQLite-only features

3. This application uses the common approach of saving an integer representing a
number of cents to store monetary amounts. The UI displays the numbers of cents
directly, rather than formatting the output in a more human-readable way

